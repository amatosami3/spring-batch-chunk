package com.exampleBatch.chunkDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ChunkDemoApplication {


	public static void main(String[] args) {
		SpringApplication.run(ChunkDemoApplication.class, args);
	}

}
