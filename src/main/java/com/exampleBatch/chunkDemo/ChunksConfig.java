package com.exampleBatch.chunkDemo;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class ChunksConfig {
 
    @Autowired
    private JobBuilderFactory jobs;
 
    @Autowired
    private StepBuilderFactory steps;
 
    @Bean
    public ItemReader<Integer> itemReader() {
        return new ReaderTest();
    }
 
    @Bean
    public ItemProcessor<Integer, Integer> itemProcessor() {
        return new ProssesorTest();
    }
 
    @Bean
    public ItemWriter<Integer> itemWriter() {
        return new WriterTest();
    }
 
    @Bean
    protected Step processLines(ItemReader<Integer> itemReader,
      ItemProcessor<Integer, Integer> processor, ItemWriter<Integer> writer) {
        return steps.get("processLines").<Integer, Integer> chunk(4)
          .reader(itemReader)
          .processor(processor)
          .writer(writer)
          .build();
    }
 
    @Bean
    public Job job() {
        return jobs
          .get("chunksJob")
          .start(processLines(itemReader(), itemProcessor(), itemWriter()))
          .build();
    }
 
}