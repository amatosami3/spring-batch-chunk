package com.exampleBatch.chunkDemo;

import org.springframework.batch.item.ItemReader;

public class ReaderTest implements ItemReader<Integer> {
	Integer i = -1;
	Integer[] o= {2,4,8,16};

	@Override
	public Integer read() throws Exception {
		
		return readFromList();
	}
	
	
	public Integer readFromList() {
		
		i++;
		if (i>o.length-1) return null;
		return o[i];
	}




}
