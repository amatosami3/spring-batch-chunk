package com.exampleBatch.chunkDemo;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

public class WriterTest implements ItemWriter<Integer> {


	@Override
	public void write(List<? extends Integer> items) throws Exception {
		System.out.println("items list after process "+items);
	}

	

}
