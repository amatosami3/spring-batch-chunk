package com.exampleBatch.chunkDemo;


import org.springframework.batch.item.ItemProcessor;

public class ProssesorTest implements ItemProcessor<Integer, Integer> {

	

	@Override
	public Integer process(Integer item) throws Exception {
		System.out.println("item before process");
		System.out.println(item);
		return item/2;
		
	}

}
